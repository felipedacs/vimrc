" felipedacs.gitlab.io

" PLUGINS :PlugUpdate
call plug#begin()
Plug 'joshdick/onedark.vim'                                         "Tema onedark. /home/neni/.vim/colors/onedark.vim
Plug 'majutsushi/tagbar'                                            "Barra lateral que mostra variáveis, funçoes objetos e etc, instalar ctags: sudo apt-get install exuberant-ctags
Plug 'jistr/vim-nerdtree-tabs'                                      "Barra lateral que mostra árvore de diretórios, instalação: git clone https://github.com/scrooloose/nerdtree.git ~/.vim/bundle/nerdtree
Plug 'vim-airline/vim-airline'                                      "Status Line
Plug 'vim-airline/vim-airline-themes'                               "Tema da status line
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }   "Instala fzf, será utilizado para encontrar arquivos
Plug 'junegunn/fzf.vim'                                             "utiliza fzf no vim
Plug 'scrooloose/nerdcommenter'                                     "snip de comentários automáticos
Plug 'cohama/lexima.vim'                                            "fechamento de ' ( [ { 
Plug 'sheerun/vim-polyglot'                                         "package de linguagens
Plug 'SirVer/ultisnips'                                             "trigger para snippets
Plug 'honza/vim-snippets'                                           "pacote de snippets das linguagens em '~/.vim/plugged/vim-snippets/snippets'
Plug 'w0rp/ale'                                                     "lint -funcionando somente com php
call plug#end()


" COMANDOS TERMINAL
set runtimepath+=~/.vim/bundle/nerdtree  "adiciona comando nerdtree
au vimenter * NERDTree                  "abre nerdtree
au vimenter * wincmd p                  "abre a janela do vim com o arquivo escolhido selecionado
au vimenter * Tagbar                    "abre tagbar


" CONFIGURAÇÕES BUILT-IN VIM
set encoding=utf-8
set termguicolors           "libera cores no terminal
colorscheme onedark         "tema main onedark, segundo é jellybeans.vim https://www.vim.org/scripts/script.php?script_id=2555 em /home/neni/.vim/colorsa
filetype plugin indent on   "identificar o arquivo e indenta
syntax on                   "ativa syntax da extensão
set tabstop=4               "tamanho identação
set softtabstop=4           "Apagar tab com o tamanho do mesmo, e não como espaço
set shiftwidth=4            "tamanho do tab
set backspace=2             "comportamento do backspace
set ai                      "auto indentação -> ==
set number                  "numero da linha atual
set relativenumber          "distancias entre a linha do cursor
highlight LineNr guifg=teal "cor do numero relativo
set mouse=a                 "libera uso do mouse em todos modos
set expandtab               "Converte tab em espaços
set showcmd                 "Mostra comandos do vim no canto inferior direito
set incsearch               "Pesquisa incremental
set ignorecase              "Ignora uppercase nas pesquisas com /search
set hls                     "Destaca resultados encontrados, para descoloir -> :noh
"set hidden                  "Buffer de undo será apagado quando um novo arquivo for aberto, mas permanesce no arquivo anterior
"mostra fechamento de {['']}
set showmatch
"pinta o match
hi! MatchParen cterm=NONE,bold gui=NONE,bold  guibg=#eee8d5 guifg=NONE
"libera cursor line
set cursorline                                                          
"cursor do normal mode ao entrar no arquivo
highlight CursorLine guifg=#444444 guibg=#98C379
"cursor durante o insert mode
autocmd InsertEnter * highlight CursorLine guifg=#444444 guibg=#61AFEF  
"cursor durante o normal mode
autocmd InsertLeave * highlight CursorLine guifg=#444444 guibg=#98C379


" CONFIGURAÇÕES STATUS LINE
let g:airline_theme='deus'  "tema


" CONFIGURAÇÕES ULTISNIPS
let g:UltiSnipsEditSplit="vertical"                                 "Abre sugestões de snippets na vertical
let g:UltiSnipsSnippetsDir = '~/.vim/plugged/vim-snippets/snippets' "Diretorio de snippets
let g:UltiSnipsExpandTrigger="<tab>"                                "trigger rápido para autocomplete
let g:UltiSnipsListSnippets="<C-@>"                                 "ctrl + espaço para ver todas opções de snippets


"CONFIGURAÇÕES NERDCOMMENTER nerdcommenter
let mapleader=","               "Comandos do nerdcommenter devem começar com vírgula: ,cc ,cu ,ca
let g:NERDSpaceDelims = 1       "adiciona/remove espaço na linha quando comentado/descomentado
let g:NERDCommentEmptyLines=1   "comenta áreas em branco


" ATALHOS
"procura por arquivos
nnoremap <c-p> :Files<cr>
"altera cursor entre nerdtree e tagbar
nnoremap <C-w> <C-w>w
"mostra nerdtree e retoma cursor para o arquivo
nnoremap <c-h> :NERDTreeTabsToggle<cr><c-w>w
"mstra tagbar
nnoremap <c-l> :TagbarToggle<cr>
"j e k mantem cursor no meio do arquivo
nnoremap j jzz
nnoremap k kzz

