set rtp+=$GOPATH/src/golang.org/x/lint/misc/vim
" felipedax
" less plugin more diy
" motivacional: https://www.youtube.com/watch?v=XA2WjJbmmoM

"######################################################
"# Built in
"######################################################
" Map leader to ,
let mapleader=',' "<leader>"

set showbreak=↳\
set encoding=utf-8
set clipboard=unnamed
filetype indent on   "identificar o arquivo e indenta
set tabstop=4               "tamanho identação
set softtabstop=4           "Apagar tab com o tamanho do mesmo, e não como espaço
set shiftwidth=4            "tamanho do tab
set backspace=2             "comportamento do backspace
set ai                      "auto indentação -> ==
set number                  "numero da linha atual
set relativenumber          "distancias entre a linha do cursor
set mouse=a                 "libera uso do mouse em todos modos
set expandtab               "Converte tab em espaços
set showcmd                 "Mostra comandos do vim no canto inferior direito
set incsearch               "Pesquisa incremental
set ignorecase              "Ignora uppercase nas pesquisas com /search
set showmatch               "mostra fechamento de {['']}
set nobk		    	    " Don't create backup files
set si				        " Turn on smart indent
set ru                      " Turn on the ruler

":h add-local-help
":h write-local-help
":h dax
helptags ~/.vim/doc
command Dax execute ":h dax"
":dax"
cnoreabbrev dax Dax
"h: local-additions

"######################################################
"# Visual Settings
"######################################################
syntax on
set nocompatible    " enter the current millenium
set termguicolors
colorscheme dracula
set ruler
set number

" tabline number
" creditos: http://vim.wikia.com/wiki/Show_tab_number_in_your_tab_line
if exists("+showtabline")
    function MyTabLine()
        let s = ''
        let t = tabpagenr()
        let i = 1
        while i <= tabpagenr('$')
            let buflist = tabpagebuflist(i)
            let winnr = tabpagewinnr(i)
            let s .= '%' . i . 'T'
            let s .= (i == t ? '%1*' : '%2*')
            let s .= i . ''
            let s .= (i == t ? '%#TabLineSel#' : '%#TabLine#')
            let file = bufname(buflist[winnr - 1])
            let file = fnamemodify(file, ':p:t')
            if file == ''
                let file = '[No Name]'
            endif
            let s .= file
            let i = i + 1
        endwhile
        let s .= '%T%#TabLineFill#%='
        let s .= (tabpagenr('$') > 1 ? '%999XX' : 'X')
        return s
    endfunction
    set stal=2
    set tabline=%!MyTabLine()
endif

"######################################################
"# statusline
"######################################################
"créditos: https://gabri.me/blog/diy-vim-statusline
"créditos: https://kadekillary.work/post/statusline/
set laststatus=2	"fixar status bar

" :h cterm
hi User1 guibg=darkred guifg=white cterm=bold
hi User2 guibg=lightgray guifg=black cterm=bold
hi User3 guibg=grey guifg=black
hi User4 guibg=darkgray guifg=black
hi User5 guibg=lightgray guifg=red cterm=bold

" plugin linter w0rp/ale
hi User6 guibg=red guifg=white cterm=bold

fun! InsertStatuslineColor()  
    hi User1 guibg=darkred guifg=white cterm=bold
    hi User2 guibg=lightgray guifg=black cterm=bold
    hi User3 guibg=Green guifg=black 
    hi User4 guibg=DarkGreen guifg=white
    hi User5 guibg=lightgray guifg=red cterm=bold
endfun

fun! InsertLeaveActions()
    hi User1 guibg=darkred guifg=white cterm=bold
    hi User2 guibg=lightgray guifg=black cterm=bold
    hi User3 guibg=grey guifg=black 
    hi User4 guibg=darkgray guifg=black 
    hi User5 guibg=lightgray guifg=red cterm=bold
endfun

au InsertEnter * call InsertStatuslineColor()
au InsertLeave * call InsertLeaveActions()

"delay entre atalhos
set ttimeout
set ttimeoutlen=100
set timeoutlen=3000

fun! CurrentGitStatus()    
    let g:gitstatus =  system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'") 
endfun
au BufEnter,BufWritePost * call CurrentGitStatus()

" Find out current buffer's size and output it.
fun! FileSize()
    let bytes = getfsize(expand('%:p'))
    if (bytes >= 1024)
        let kbytes = bytes / 1024
    endif
    if (exists('kbytes') && kbytes >= 1000)
        let mbytes = kbytes / 1000
    endif

    if bytes <= 0
        return '0'
    endif

    if (exists('mbytes'))
        return mbytes . 'MB '
    elseif (exists('kbytes'))
        return kbytes . 'KB '
    else
        return bytes . 'B '
    endif
endfun

set statusline+=
set statusline+=\%1*\ %{g:gitstatus}\ %*	" branch
set statusline+=\%5*\ %m\%2*\%f\ %* 	    " nome abreviado/teste readonly/modificado/
set statusline+=\%3*\ %F					" full path
set statusline+=%=						    " Espaço
set statusline+=\%4*\ %{FileSize()}			" Tamanho arquivo
set statusline+=\%4*\ %p%%\ %l:\%c          " Rownumber/total (%)

"######################################################
"# Abbreviations
"######################################################
"" no one is really happy until you have this shortcuts
cnoreabbrev W! w!
cnoreabbrev Q! q!
cnoreabbrev Qall! qall!
cnoreabbrev Wq wq
cnoreabbrev Wa wa
cnoreabbrev wQ wq
cnoreabbrev WQ wq
cnoreabbrev W w
cnoreabbrev Q q
cnoreabbrev Qall qall
":ajuda para abrir markdown com pandoc para html com comandos vim
cnoreabbrev ajuda <Esc>:w<CR>:!clear;pandoc ~/.vim/vim_help.md -s -o ~/.vim/vim_help.html;firefox ~/.vim/vim_help.html<CR>

" parenteses, teclado quebrado
:inoremap ¢ ()<left>
" substituindo lexim.vim
:inoremap " ""<left>
:inoremap ' ''<left>
:inoremap [ []<left>
:inoremap { {}<left>
:inoremap < <><left>

"######################################################
"# Mappings
"######################################################
"procura por arquivos com plug fzf
nnoremap <c-p> :tabfind 
"acabar com o ctrl z
nnoremap <C-Z> u
nnoremap <c-z> u

"j e k mantem cursor no meio do arquivo
nnoremap j jzz
nnoremap k kzz

"" Execuções
nnoremap <leader>py <Esc>:w<CR>:!clear;python3 %<CR>
nnoremap <leader>gr <Esc>:w<CR>:!clear;go run %<CR>
nnoremap <leader>gc <Esc>:w<CR>:!clear;gcc % -o executavel ; ./executavel ; rm executavel<CR>

"" Split
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>

"" Switching windows
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l
noremap <C-h> <C-w>h

"######################################################
"# Autocomplete
"######################################################
"créditos: https://github.com/changemewtf/no_plugins
set wildmenu
set wildmode=list:full
set completeopt=longest,menuone,preview " cool completion view
set completeopt+=noselect               " deixar digitar
set wildchar=<Tab>                      " (default)
set omnifunc=syntaxcomplete#Complete    "ctrl-x ctrl-o
set complete=.,w,b,u,t

" arquivo
imap <C-f> <C-x><C-f>

"dicionario
imap <C-k> <C-x><C-k>

"palavras chave da linguagem
imap <C-o> <C-x><C-o>

" Tab para wildmode
" créditos: https://gist.github.com/felipedax/51a55db4a04d1b82f72fe639eb6fcf9f
inoremap <expr> <Tab> TabComplete()
fun! TabComplete()
    if getline('.')[col('.') - 2] =~ '\K' || pumvisible()
        return "\<down>"
    else
        return "\<Tab>"
    endif
endfun

"######################################################
"# Snippets
"######################################################
au FileType go,html call LoadFileTypeDefaults()

set hls    "Destaca resultados encontrados, para descolorir -> :noh

" cria/carregar dicionario e snippets de acordo com a linguagem
fun! LoadFileTypeDefaults()
    " path dir snippets
    let pathDirSnippets = "~/.vim/snippets/"
    " path do dicionario
    let dict = pathDirSnippets.&ft.".dictionary.txt"
    " path snippets
    let pathSnippets = pathDirSnippets.&ft.".snippets.txt"
    " path da variável que centraliza dicionario/iabbr
    let pathVarSnippets = pathDirSnippets.&ft.".vim"


    " ordem:
    " *Descobrir títulos snippets
    " *Agrupar títulos em um array para que possam ser iterados
    " *Iterar entre titulos para:
    "   *Escrever no dicionario
    "   *Criar abreviação e insert [iabbr] para chamálos


    " O primeiro passo é descobrir toas iabbr dos snippets
    " Abrir arquivo da variável e resetar
    execute ":tabfind ".pathVarSnippets
    execute "normal! ggVGD"
    :wq

    " Abrir arquivo dos snippets e copiar todos títulos de snippets
    execute ":tabfind ".pathSnippets  
    execute "normal! qaq"
    execute ":g/{{%[^%]*%}}/y A"

    " Abrir arquivo da variável novamente para colar os títulos e formatá-los como array
    execute ":vs ".pathVarSnippets
    execute "normal! p"
    :%s/{{%/"/g
    :%s/%}}/"/g
    :%s/"\n"/","/g
    :echo "Padrão não encontrado: '\\n' há somente um snippet. Não é um problema!"
    normal! Ilet g:snips=[
    normal! A]
    " salva e fecha os arquivos
    :wq
    :q!
    " adicionar a variável ao vimrc
    execute "source ".pathVarSnippets


    " Agora é a configuração do dicionário e do iabbr
    " abre dicionario para escrever os títulos dos snippets
    execute ":tabfind ".dict
    " variável com o comando completo para escrever dicionario
    let escreveDicionario = "normal! ggVGs"

    " itera pela variável de pathVarSnippets
    for ia in g:snips
        " concatena cada palavra nova com quebra de linha
        let escreveDicionario .= ia."\n"

        " cria iabbr com base no snip
        execute ":ia ".ia." <esc><esc>:-1r ".pathSnippets."<cr><esc>/{%InicioSnippets%}<cr>v/{%FinalSnippets%}<cr>==<esc>/{%InicioSnippets%}<cr>v/{{%".ia."%}}<cr>D/endsnippet<cr>v/{%FinalSnippets%}<cr>Ddd/{%[^%]*%}<cr>zz:echo 'digite ;; para substituição'"
    endfor

    " executa a string como comando 
    :execute escreveDicionario
    " salva/fecha dicionario
    :wq
    " seta dicionario
    exe "set dictionary+=".dict
endfun

" completar snippet
noremap ;; <Esc>/{%[^%]*%}<CR>ggnzzc%
inoremap ;; <Esc>/{%[^%]*%}<CR>ggnzzc%

"######################################################
"# File browsing
"######################################################
" - :edit a folder to open a file browser
" - <CR>/v/t to open in an h-split/v-split/tab
" - check |netrw-browse-maps| for more mappings
let g:netrw_banner=0        " disable annoying banner
let g:netrw_browse_split=4  " open in prior window
let g:netrw_altv=1          " open splits to the right
let g:netrw_liststyle=3     " tree view

" toggle para mostrar diretórios
let g:tnet = 1
fun! ToggleGerenciador()
    if(g:tnet==1)
        :vs
        :edit .
        let g:tnet = 0
    else
        :q!
        let g:tnet = 1
    endif
endfun

nnoremap <space> :call ToggleGerenciador()<cr>


"######################################################
"# Plugins
"######################################################

"source ~/.vimrc.plugins

" vim-bootstrap b990cad and custom felipedax

"*****************************************************************************
"" Vim-PLug core
"*****************************************************************************
if has('vim_starting')
  set nocompatible               " Be iMproved
endif

let vimplug_exists=expand('~/.vim/autoload/plug.vim')

if !filereadable(vimplug_exists)
  if !executable("curl")
    echoerr "You have to install curl or first install vim-plug yourself!"
    execute "q!"
  endif
  echo "Installing Vim-Plug..."
  echo ""
  silent !\curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  let g:not_finish_vimplug = "yes"

  autocmd VimEnter * PlugInstall
endif

"*****************************************************************************
"" Plug install packages
"*****************************************************************************
" Required:
call plug#begin(expand('~/.vim/plugged'))

"=========================
" geral plugins
"=========================

Plug 'majutsushi/tagbar'                    "Barra lateral que mostra variáveis, funçoes objetos e etc, instalar ctags: sudo apt-get install exuberant-ctags
Plug 'w0rp/ale'                             "sinalizador de linter 
Plug 'tpope/vim-fugitive'                   "git no vim
Plug 'gregsexton/gitv', {'on': ['Gitv']}    "indicador das branchs e commits, necessecita do vim-fugitive. Talvez tenha que clonar manualmente em plugged
Plug 'airblade/vim-gitgutter'               "indicador +- git
Plug 'sheerun/vim-polyglot'
Plug 'othree/html5.vim'
"=========================
" go plugins
"=========================
" go lint: gofmt


"=========================
" javascript plugins
"=========================



"=========================
" php plugins
"=========================



"=========================
" python plugins
"=========================


call plug#end()

"*****************************************************************************
"" Autocmd Rules
"*****************************************************************************
"" Remember cursor position
augroup vimrc-remember-cursor-position
  autocmd!
  autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
augroup END

"*****************************************************************************
"" Custom configs plugins/langs
"*****************************************************************************
"=========================
" geral plugins
"=========================

"--------------
" tagbar
"--------------
map <bs> :Tagbar<cr>


"--------------
" gitgutter
"--------------
" gitgutter
" <leade>hp -> ver diferenças
" [c -> previous hunk   ]c -> next hunk
set updatetime=100 "atualização mais rápida de +-


"--------------
" ale
"--------------
function! LinterStatus() abort
    let l:counts = ale#statusline#Count(bufnr(''))

    let l:all_errors = l:counts.error + l:counts.style_error
    let l:all_non_errors = l:counts.total - l:all_errors

    return l:counts.total == 0 ? 'OK' : printf(
    \   '%dW %dE',
    \   all_non_errors,
    \   all_errors
    \)
endfunction

let g:ale_echo_msg_error_str = 'E'
let g:ale_echo_msg_warning_str = 'W'
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'

nmap <silent> <leader>ap <Plug>(ale_previous_wrap)
nmap <silent> <leader>an <Plug>(ale_next_wrap)

set statusline+=\%6*\%{LinterStatus()}


"=========================
" go
"=========================
